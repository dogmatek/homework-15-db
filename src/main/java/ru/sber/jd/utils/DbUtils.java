package ru.sber.jd.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtils {
    public static Connection createConnection() {
        try{
            return DriverManager.getConnection("jdbc:h2:~/hwsql;MODE=PostgreSQL", "sa", "");
        } catch (SQLException throwables){
            throwables.printStackTrace();
            return null;
        }
    }
}
