package ru.sber.jd.fakedao;


import ru.sber.jd.entities.CreditAgreementEntity;
import ru.sber.jd.utils.DbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class FakeCreditAgreementDao implements Fake {
    private final String CREATE_CREDITAGREEMENT_TABLE = "create table credit (id int, client_id int, number text, credit int)";
    private final String DROP_CREDITAGREEMENT_TABLE = "drop table if exists credit";
    private final String INSERT_CREDITAGREEMENT_TABLE = "insert into credit (id, client_id, number, credit) values (?, ?, ?, ?)";
    private final Connection connection = DbUtils.createConnection();



    private Integer id;
    private Integer client_id;  // ID Клиента
    private String number;      // Номер по системе банка
    private Integer credit;     // Сумма долга

    @Override
    public void createTable() throws SQLException {
        PreparedStatement preparedStatementdrop =  connection.prepareStatement(DROP_CREDITAGREEMENT_TABLE);
        preparedStatementdrop.execute();
        connection.commit();

        PreparedStatement preparedStatement =  connection.prepareStatement(CREATE_CREDITAGREEMENT_TABLE);
        preparedStatement.execute();
        connection.commit();
    }

    @Override
    public void fillTable() throws SQLException {
        int i = 0;
        List<CreditAgreementEntity> entities = Arrays.asList(
                CreditAgreementEntity.builder()
                        .id(i)
                        .clientId(Math.abs(new Random().nextInt()) % 5)
                        .number("КД/"+ i )
                        .credit(1000000)
                        .build(),
                CreditAgreementEntity.builder()
                        .id(++i)
                        .clientId(Math.abs(new Random().nextInt()) % 5)
                        .number("КД/"+ i )
                        .credit(2000000)
                        .build(),
                CreditAgreementEntity.builder()
                        .id(++i)
                        .clientId(Math.abs(new Random().nextInt()) % 5)
                        .number("КД/"+ i )
                        .credit(1500000)
                        .build(),
                CreditAgreementEntity.builder()
                        .id(++i)
                        .clientId(Math.abs(new Random().nextInt()) % 5)
                        .number("КД/"+ i )
                        .credit(3000000)
                        .build(),
                CreditAgreementEntity.builder()
                        .id(++i)
                        .clientId(Math.abs(new Random().nextInt()) % 5)
                        .number("КД/"+ i )
                        .credit(4000000)
                        .build()
        );

        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CREDITAGREEMENT_TABLE);

        for (CreditAgreementEntity entity : entities){
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setInt(2, entity.getClientId());
            preparedStatement.setString(3, entity.getNumber());
            preparedStatement.setInt(4, entity.getCredit());
            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
        connection.commit();

    }
}
