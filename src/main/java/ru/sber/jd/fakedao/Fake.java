package ru.sber.jd.fakedao;

import java.sql.SQLException;

public interface Fake {
    void createTable() throws SQLException;
    void fillTable() throws SQLException;
}
