package ru.sber.jd.fakedao;


import ru.sber.jd.entities.CreditAgreementEntity;
import ru.sber.jd.utils.DbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class FakeClientDao implements Fake {
    private final String CREATE_CLIENT_TABLE = "create table client (id int, name text, rating int)";
    private final String DROP_CLIENT_TABLE = "drop table if exists client";
    private final Connection connection = DbUtils.createConnection();

    @Override
    public void createTable() throws SQLException {
        PreparedStatement preparedStatementdrop =  connection.prepareStatement(DROP_CLIENT_TABLE);
        preparedStatementdrop.execute();
        connection.commit();

        PreparedStatement preparedStatement =  connection.prepareStatement(CREATE_CLIENT_TABLE);
        preparedStatement.execute();
        connection.commit();
    }

    @Override
    public void fillTable() throws SQLException {

    }
}
