package ru.sber.jd.fakedao;

import ru.sber.jd.entities.CreditAgreementEntity;
import ru.sber.jd.entities.PledgeAgreementEntity;
import ru.sber.jd.utils.DbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class FakePledgeAgreementDao  implements Fake {
    private final String CREATE_PLEDGEAGREEMENT_TABLE =
            "create table pledge (id int, client_id int, number text,description text, collateral_value int)";
    private final String DROP_PLEDGEAGREEMENT_TABLE = "drop table if exists pledge";
    private final String INSERT_PLEDGEAGREEMENT_TABLE = "insert into pledge (id, client_id, number, description, collateral_value) values (?, ?, ?, ?, ?)";
    private final Connection connection = DbUtils.createConnection();

    @Override
    public void createTable() throws SQLException {
        PreparedStatement preparedStatementdrop =  connection.prepareStatement(DROP_PLEDGEAGREEMENT_TABLE);
        preparedStatementdrop.execute();
        connection.commit();

        PreparedStatement preparedStatement =  connection.prepareStatement(CREATE_PLEDGEAGREEMENT_TABLE);
        preparedStatement.execute();
        connection.commit();
    }

    @Override
    public void fillTable() throws SQLException {
        int i = 0;
        List<PledgeAgreementEntity> entities = Arrays.asList(
                PledgeAgreementEntity.builder()
                        .id(i)
                        .clientId(Math.abs(new Random().nextInt()) % 5)
                        .number("ДЗ/"+ +i +"/245/" + Math.abs(new Random().nextInt()) % 100 +"/"+ Math.abs(new Random().nextInt()) % 10  )
                        .description("Залог 2 единиц оборудования")
                        .CollateralValue((Math.abs(new Random().nextInt()) % 10) * 100000 )
                        .build(),
                PledgeAgreementEntity.builder()
                        .id(++i)
                        .clientId(Math.abs(new Random().nextInt()) % 5)
                        .number("ДЗ/"+ +i)
                        .description("Залог офисного здания")
                        .CollateralValue((Math.abs(new Random().nextInt()) % 10) * 100000 )
                        .build(),
                PledgeAgreementEntity.builder()
                        .id(++i)
                        .clientId(Math.abs(new Random().nextInt()) % 5)
                        .number("ДЗ/"+ +i)
                        .description("ипотека 2-комнатной квартиры")
                        .CollateralValue((Math.abs(new Random().nextInt()) % 10) * 100000 )
                        .build(),
                PledgeAgreementEntity.builder()
                        .id(++i)
                        .clientId(Math.abs(new Random().nextInt()) % 5)
                        .number("ДЗ/"+ +i)
                        .description("Залог 1 единицы транспорта")
                        .CollateralValue((Math.abs(new Random().nextInt()) % 10) * 100000 )
                        .build(),
                PledgeAgreementEntity.builder()
                        .id(++i)
                        .clientId(Math.abs(new Random().nextInt()) % 5)
                        .number("ДЗ/"+ +i)
                        .description("Залог дорожной техники")
                        .CollateralValue((Math.abs(new Random().nextInt()) % 10) * 100000 )
                        .build()

        );

        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PLEDGEAGREEMENT_TABLE);

        for (PledgeAgreementEntity entity : entities){
            preparedStatement.setInt(1, entity.getId());
            preparedStatement.setInt(2, entity.getClientId());
            preparedStatement.setString(3, entity.getNumber());
            preparedStatement.setString(4, entity.getDescription());
            preparedStatement.setInt(5, entity.getCollateralValue());
            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
        connection.commit();

    }
}
