package ru.sber.jd.fakedao;

import ru.sber.jd.entities.CreditPledge;
import ru.sber.jd.utils.DbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FakeCreditPledgeDao implements Fake {
    private final String CREATE_CREDITPLEDGE_TABLE = "create table credit_pledge (credit_id int, pledge_id int)";
    private final String DROP_CREDITPLEDGE_TABLE = "drop table if exists credit_pledge";
    private final String INSERT_CREDITPLEDGE_TABLE = "insert into credit_pledge (credit_id, pledge_id) values (?, ?)";
    private final Connection connection = DbUtils.createConnection();

    @Override
    public void createTable() throws SQLException {
        PreparedStatement preparedStatementdrop =  connection.prepareStatement(DROP_CREDITPLEDGE_TABLE);
        preparedStatementdrop.execute();
        connection.commit();

        PreparedStatement preparedStatement =  connection.prepareStatement(CREATE_CREDITPLEDGE_TABLE);
        preparedStatement.execute();
        connection.commit();
    }

    @Override
    public void fillTable() throws SQLException {
        int i = 0;
        List<CreditPledge> entities = new ArrayList<>();

        for (int j = 0; j < 20; j++) {
            entities.add(CreditPledge.builder()
                    .сreditId(Math.abs(new Random().nextInt()) % 5)
                    .pledgeId(Math.abs(new Random().nextInt()) % 5)
                    .build());
        }
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CREDITPLEDGE_TABLE);

        for (CreditPledge entity : entities){
            preparedStatement.setInt(1, entity.getСreditId());
            preparedStatement.setInt(2, entity.getPledgeId());
            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
        connection.commit();


        preparedStatement = connection.prepareStatement(
                "select * from credit_pledge");
        ResultSet resultSet = preparedStatement.executeQuery();

//        while (resultSet.next()) System.out.println(resultSet.getInt(1) + ", " + resultSet.getInt(2));

    }

}
