package ru.sber.jd.dao;


import ru.sber.jd.entities.ClientEntity;
import ru.sber.jd.utils.DbUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientDao {
    private final Integer ID_COLUMN = 1;
    private final Integer NAME_COLUMN = 2;
    private final Integer RATING_COLUMN = 3;
    private final Integer CREDITNUM_COLUMN = 4;
    private final Integer CREDITVALUE_COLUMN = 5;
    private final Integer PLEDGENUM_COLUMN = 6;
    private final Integer PLEDGER_COLUMN = 7;
    private final Integer PLEDGEDESCRIPTION_COLUMN = 8;
    private final Integer PLEDGEDECOLLATERAL_VALUE_COLUMN = 9;

    private final String INSERTSQL = "insert into client (id, name, rating) values (?, ?, ?)";

    /*
        Выбрать все кредиты клиента с указанием договор залоги и алогодателя
        Отсортировать по номеру кредитного договора
        Примечание:
        Залогодатель и заёмщик не обяательно одно лицо
        1 договор залога может обеспечивать несколько кредитных договоров
        Несколько договор залога могут обеспечивать 1 кредитных договор
     */
    private final String SELECTSQL = "select client.id, client.name, client.rating," +
            " credit.number, credit.credit," +
            " pledge.number, cl.name, pledge.description, pledge.collateral_value" +
            " from client" +
            " inner join credit on client.id = credit.client_id" +
            " left join credit_pledge on credit.id = credit_pledge.credit_id" +
            " left join pledge on credit_pledge.pledge_id = pledge.id" +
            " left join client as cl on pledge.client_id = cl.id"
            + " where client.id = 2 order by credit.number";

    /*
        Выбрать все кредитные договоры, по которым заёмщик и залогодатель одно лицо
     */
    private final String SELECTSQL1 = "select client.id, client.name, client.rating," +
            " credit.number, credit.credit," +
            " pledge.number, cl.name, pledge.description, pledge.collateral_value" +
            " from client" +
            " inner join credit on client.id = credit.client_id" +
            " left join credit_pledge on credit.id = credit_pledge.credit_id" +
            " left join pledge on credit_pledge.pledge_id = pledge.id" +
            " left join client as cl on pledge.client_id = cl.id"
            + " where client.id = pledge.client_id order by credit.number";



    private final Connection connection = DbUtils.createConnection();

    public List<ClientEntity> selectAll() throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement(SELECTSQL);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<ClientEntity> entities = new ArrayList<>();
        while (resultSet.next()){
            entities.add(ClientEntity.builder()
                    .id(resultSet.getInt(ID_COLUMN))
                    .name(resultSet.getString(NAME_COLUMN))
                    .rating(resultSet.getInt(RATING_COLUMN))

                    .numCredit(resultSet.getString(CREDITNUM_COLUMN))
                    .creditValue(resultSet.getInt(CREDITVALUE_COLUMN))

                    .numPledge(resultSet.getString(PLEDGENUM_COLUMN))
                    .Pledger(resultSet.getString(PLEDGER_COLUMN))
                    .descriptionPledge(resultSet.getString(PLEDGEDESCRIPTION_COLUMN))
                    .CollateralValue(resultSet.getInt(PLEDGEDECOLLATERAL_VALUE_COLUMN))
                    .build());
        }
        return entities;
    }

    public void insertClients(List<ClientEntity> etities) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERTSQL);

        for (ClientEntity etity : etities){
            preparedStatement.setInt(ID_COLUMN, etity.getId());
            preparedStatement.setString(NAME_COLUMN, etity.getName());
            preparedStatement.setInt(RATING_COLUMN, etity.getRating());
            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
        connection.commit();
    }

}
