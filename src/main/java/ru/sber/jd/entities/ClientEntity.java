package ru.sber.jd.entities;


import lombok.Builder;
import lombok.Value;

/*
    Клиент
 */

@Builder
@Value
public class ClientEntity {
    private Integer id;
    private String name;        // Наименование
    private Integer rating;     // Рейтинг клиента

    // Зарезервированные переменные
    private String numCredit;           // номер кредита
    private Integer creditValue;        // Сумма кредита

    private String numPledge;           // номер Договора залога
    private String Pledger;             // Залогодатель
    private String descriptionPledge;   // Описание обеспечения
    private Integer CollateralValue;    // Залоговая стоимость
}
