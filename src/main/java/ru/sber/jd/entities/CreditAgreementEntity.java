package ru.sber.jd.entities;

/*
    Кредитный договор
 */

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class CreditAgreementEntity {
    private Integer id;
    private Integer clientId;  // ID Клиента
    private String number;      // Номер по системе банка
    private Integer credit;     // Сумма долга

}
