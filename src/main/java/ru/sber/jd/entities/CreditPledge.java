package ru.sber.jd.entities;

/*
        Связь Кредитный договор - договор залог
 */

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class CreditPledge {
    private Integer сreditId;
    private Integer pledgeId;
}
