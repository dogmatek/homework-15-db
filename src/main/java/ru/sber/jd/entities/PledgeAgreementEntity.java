package ru.sber.jd.entities;

import lombok.Builder;
import lombok.Value;

/*
    Договор залога
 */
@Builder
@Value
public class PledgeAgreementEntity {
    private Integer id;
    private Integer clientId;          // ID Клиента
    private String number;              // Номер по системе банка
    private String description;         // Описание залога
    private Integer CollateralValue;    // Залоговая стоимость
}
