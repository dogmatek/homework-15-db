package ru.sber.jd;

import ru.sber.jd.dao.ClientDao;
import ru.sber.jd.entities.ClientEntity;
import ru.sber.jd.fakedao.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Application {
    private static List<Fake> fake = Arrays.asList(new FakeClientDao(), new FakeCreditAgreementDao(),
            new FakePledgeAgreementDao(), new FakeCreditPledgeDao()
    );

    private static ClientDao clientDao = new ClientDao();

    public static void main(String[] args) throws SQLException {
        init();


        List<ClientEntity> etities = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            etities.add(ClientEntity.builder()
                    .id(i)
                    .name("Клиент-" + Math.abs(new Random().nextInt()) % 100)
                    .rating(5 + Math.abs(new Random().nextInt()) % 20 )
                    .build());
        }

        clientDao.insertClients(etities);

        System.out.println("Все кредиты клиента 2:");
        for (ClientEntity entity : clientDao.selectAll() ) {
            System.out.println(entity);
        }
        System.out.println("-----------------------------------");


    }

    public static void init(){
        fake.stream().forEach( f -> {
            try {
                f.createTable();
                f.fillTable();

            } catch (SQLException ex) {
                throw new RuntimeException("Дальнейшая работа не возможна", ex);
            }
        });
    }
}